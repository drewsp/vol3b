from mpi4py import MPI
from sys import argv
import numpy as np

"""
Write a script that runs a Monte Carlo simulation to approximate the volume
of an m-dimensional unit sphere.  Your script should accept the following
parameters as command line arguments:
    n - number of processes (passed as normal into mpirun)
    m - dimension of unit sphere
    p - number of points to use in simulation

For your final answer, print out the approximated volume from your simulation.
"""
COMM = MPI.COMM_WORLD
RANK = MPI.COMM_WORLD.Get_rank()
number_processes = COMM.Get_size()
m = int(argv[1])
p = int(argv[2])

my_array = np.random.rand(p,m)
normed = np.linalg.norm(my_array,axis = 1)

norm_greater_1 = np.sum(normed < 1)
percent_in = float(norm_greater_1)/p

volume = 2.**(m)*percent_in

vol = COMM.gather(volume, root=0)
if RANK == 0:
	average = np.mean(np.array(vol))
	print average
