from mpi4py import MPI
from sys import argv
import numpy as np

"""
Write a script that runs on n processes and passes an n by 1 array
of random values from the ith process to the i+1th process (or to the 0th
process for the if i is the last process).  Have each process identify
itself and what it is doing:

    e.g. "Process {process_num} sending array {array to process_num + 1}"
    and "Process {process_num} just received {array from process_num - 1}"
"""

COMM = MPI.COMM_WORLD
RANK = MPI.COMM_WORLD.Get_rank()
number_processes = COMM.Get_size()
n = int(argv[1])
num_buffer = np.random.rand(n,1)


print "Process {} Sending: {} to process {}".format(RANK, num_buffer, (RANK+1)%number_processes)
COMM.Send(num_buffer, dest=(RANK+1)%number_processes)
print "Process {}: Message sent.".format(RANK)
num_buffer = np.zeros((n,1))

print "Process {}: Waiting for the message... current num_buffer={}.".format((RANK+1)%number_processes,num_buffer)
COMM.Recv(num_buffer, source=(RANK-1)%number_processes)
print "Process {}: Message recieved! num_buffer={}.".format((RANK+1)%number_processes, num_buffer)





