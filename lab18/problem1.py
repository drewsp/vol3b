from mpi4py import MPI

"""
Write a program that prints "Hello from processor ____" for even processors
and "Goodbye from processor ____" for odd processors
"""

RANK = MPI.COMM_WORLD.Get_rank()


if RANK%2 == 0:
	print 'Hello from processor {}.'.format(RANK)
else:
	print 'Goodbye from processor {}.'.format(RANK)
