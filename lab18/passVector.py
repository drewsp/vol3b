from mpi4py import MPI
from sys import argv
import numpy as np


"""
Write a script that runs on two processes and passes an n by 1 array
of random values from one process to the other.  Have each process identify
itself and what it is doing:

    e.g. "Process 1 sending array {array}"
    and "Process 0 just received {array}"
"""
COMM = MPI.COMM_WORLD
RANK = MPI.COMM_WORLD.Get_rank()
# Get n passed in by user
n = int(argv[1])


if RANK == 1: # This process chooses and sends a random value
	num_buffer = np.random.rand(n,1)
	print "Process 1: Sending: {} to process 0.".format(num_buffer)
	COMM.Send(num_buffer, dest=0)
	print "Process 1: Message sent."
elif RANK ==0:
	num_buffer = np.zeros((n,1))
	print "Process 0: Waiting for the message... current num_buffer={}.".format(num_buffer)
	COMM.Recv(num_buffer, source=1)
	print "Process 0: Message recieved! num_buffer={}.".format(num_buffer)
