"""Volume III: Web Scraping 2.
Drew Pearson
Math 405
January 31, 2017
"""
import re
from bs4 import BeautifulSoup
import pandas as pd
import urllib2
from urlparse import urljoin
import matplotlib.pyplot as plt

# Problem 1
def Prob1():
    """Load the Big Bank Info file and return a pandas DataFrame
    containing Bank Name, Rank, ID, Domestic Assets, and Domestic Branches
    for JP Morgan, Capital One, and Discover banks.
    """
    soup = BeautifulSoup(open('federalreserve.htm'),'html.parser')
    columns = [elem.find('b').text for elem in soup.find_all('td', {'scope': 'COL'})]
    banks = soup.find_all('tr', {'valign': 'TOP'})
    banks = [[elem.text for elem in row.find_all('td')] for row in banks]
    bankpd = pd.DataFrame(banks, columns = columns)
    bankpd = bankpd[['Bank Name / Holding Co Name', 'Natl Rank', 'Bank ID', 'Domestic Assets', 'Domestic Branches']]
    bankpd = bankpd.loc[bankpd['Bank Name / Holding Co Name'].isin(['CAPITAL ONE BK/', 'JPMORGAN CHASE BK/J P MORGAN CHASE & CO', 'DISCOVER BK/MORGAN STANLEY'])]
    return bankpd


# Problem 2
def Prob2():
    """Use urllib2 and BeautifulSoup to return the actual max temperature,
    the tag containing the link for 'Next Day', and the url associated
    with that link.
    """
    url = 'https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo='
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content, 'html.parser')
    max_date = soup.find('td', text = re.compile('Max Temperature')).parent.find_all('td')[1].span.span.text
    next_day_tag = soup.find('div', {'class': 'next-link'}).find('a')
    next_day_url = urljoin(url, next_day_tag['href'])
    return max_date, next_day_tag, next_day_url
# Problem 3
def Prob3():
    """Mimic the Wunderground Weather example to create a list of average
    max temperatures of the year 2015 in San Diego. Use matplotlib to draw
    a graph depicting the data, then return the list.
    """
    url = 'https://math.byu.edu/weather/www.wunderground.com/history/airport/KSLC/2015/1/1/DailyHistory.html?req_city=&req_state=&req_statename=&reqdb.zip=&reqdb.magic=&reqdb.wmo='
    content = urllib2.urlopen(url).read()
    soup = BeautifulSoup(content, 'html.parser')
    avg_max_temps = []

    while('January' in soup.find(class_='history-date').string or 'February' in soup.find(class_='history-date').string):
        place = soup.find('thead').find_all('th').index(soup.find('th', text = re.compile('Average')))
        max_date = soup.find('td', text = re.compile('Max Temperature')).parent.find_all('td')[place].span.span.text
        next_day_tag = soup.find('div', {'class': 'next-link'}).find('a')
        url = urljoin(url, next_day_tag['href'])
        avg_max_temps.append(max_date)
        content = urllib2.urlopen(url).read()
        soup = BeautifulSoup(content, 'html.parser')

    plt.plot(range(len(avg_max_temps)), avg_max_temps)
    plt.xlabel('Day')
    plt.ylabel('Temperature')
    plt.show()
    return avg_max_temps





# Problem 4
def Prob4():
    """Load the selected option into BeautifulSoup. Find the requested
    information for the selected option and make a SQL table that
    stores this information.
    """
    soccer = pd.DataFrame()
    columns = ['name', 'hometown', 'position', 'games_played']
    soccer = pd.DataFrame(columns = columns)
    base_url =  'http://www.foxsports.com'
    content = urllib2.urlopen(base_url+'/soccer/united-states-women-team-stats').read()
    soup = BeautifulSoup(content, 'html.parser')
    table_container = soup.find('div', {"class" : "wisbb_expandableTable wisbb_playerFixed wisbb_statsTable"})
    table = table_container.find('table')
    table_body = table.find('tbody')
    player_list = [row.find('a')['href'] for row in table_body.find_all('tr') if row.find('a') is not None]
    games_played = [row.find_all('td')[1].text for row in table_body.find_all('tr') if row.find_all('td') is not None]
    games_played.remove('')
    games_played.remove('')
    #print player_list
    #print games_played
    #print len(player_list), len(games_played)
    for i, player in enumerate(player_list):
        content2 = urllib2.urlopen(base_url+player).read()
        player_soup = BeautifulSoup(content2, 'html.parser')
        first_name = player_soup.find('div', {"class" : "wisfb_bioLargeName"}).find('h1').text
        last_name = player_soup.find('div', {"class" : "wisfb_bioLargeName"}).find('h1').find('span').text
        hometown = player_soup.find('div', {'class' : 'wisfb_bioDataContainer'}).find('table', {'class':'wisfb_playerData'}).find_all('tr')[1].find_all('td')[1].text
        position = player_soup.find('span', {'class' : 'wisfb_bioLargeSubInfo'}).text.split("|")[0]
        data = [first_name, hometown, position, games_played[i]]
        #print data 
        soccer.loc[i] = data
    return soccer



# Problem 5
def Prob5():
    """Use selenium to return a list of all the a tags containing each of the
    30 NBA teams. Return only one tag per team.
    """
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.wait import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    url = 'http://stats.nba.com/teams/traditional/#!?sort=W_PCT&dir=-1'

    driver = webdriver.Firefox()
    driver.get(url)
    wait = WebDriverWait(driver, 60)
    wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, '.nba-stat-table')))
    soup = BeautifulSoup(driver.page_source)
    tags = soup.find('div', {"class" : "nba-stat-table__overlay"}).find_all('td',{'class':'first'})
    teams = [tag.find('a') for tag in tags]
    ####driver.quit()
    return teams


if __name__ == '__main__':
    #print Prob1()
    #print Prob2()
    #print Prob4()
    print Prob5()






