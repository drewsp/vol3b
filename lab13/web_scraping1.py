"""Volume 3B: Web Scraping 1. Spec file."""
import re
from bs4 import BeautifulSoup
html_doc = """
    <html><head><title>The Three Stooges</title></head>
    <body>
    <p class="title"><b>The Three Stooges</b></p>
    <p class="story">Have you ever met the three stooges? Their names are
    <a href="http://example.com/larry" class="stooge" id="link1">Larry</a>,
    <a href="http://example.com/mo" class="stooge" id="link2">Mo</a> and
    <a href="http://example.com/curly" class="stooge" id="link3">Curly</a>;
    and they are really hilarious.</p>
    <p class="story">...</p>
    """

# Problem 1
def prob1(filename):
    """
    Find all of the tags used in a particular .html file and the value of
    the 'type' attribute.

    Inputs:
        filename: Name of .html file to parse

    Outputs:
        - A set of all of the tags used in the .html file
        - The value of the 'type' attributes for the style tag
    """
    with open('example.htm') as myfile:
        example = myfile.read()
        soup = BeautifulSoup(example, 'html.parser')
    tags_used = [tag.name for tag in soup.find_all()]
    tag = soup.style
    value = str(tag['type'])
    return set(tags_used), value

# Problem 2
def prob2():
    """Prints (not returns) the prettified
    string for the Three Stooges HTML.
    """
    soup = BeautifulSoup(html_doc, 'html.parser')
    print soup.prettify()

# Problem 3
def prob3():
    """Returns [u'title'] from the Three Stooges soup"""
    soup = BeautifulSoup(html_doc, 'html.parser')
    tag = soup.p

    return tag.string

# Problem 4
def prob4():
    """Returns u'Mo' from the Three Stooges soup"""
    soup = BeautifulSoup(html_doc, 'html.parser')
    for child in soup.descendants:
        if child.string == 'Mo':
            return child.string
        else:
            pass


# Problem 5
def prob5(method):
    """Returns the u'More information...' using two different methods.
    If method is 1, it uses first method. If method is 2, it uses
    the second method.
    """
    example_soup = BeautifulSoup(open('example.htm'), 'html.parser')
    if method == 1:
        for child in example_soup.descendants:
            if child.string == 'More information...':
                return child.string
            else:
                pass
    if method == 2:
        return example_soup.a.string

# Problem 6
def prob6(method):
    """Returns the tag associated with the "More information..."
    link using two different methods. If method is 1, it uses the
    first method. If method is 2, it uses the second method.
    """
    example_soup = BeautifulSoup(open('example.htm'), 'html.parser')
    if method == 1:
        return example_soup.find(text = 'More information...').parent
    if method == 2:
        return example_soup.a

# Problem 7
def prob7():
    """Loads 'SanDiegoWeather.htm' into BeautifulSoup and prints
    (not returns) the tags referred to the in the Problem 7 questions.
    """
    soup = BeautifulSoup(open('SanDiego.htm'), 'html.parser')
    soup.find(text = 'Thursday, January 1, 2015').parent
    # Question 1
    print soup.find(text = 'Thursday, January 1, 2015').parent
    # Question 2
    print soup.find(text = re.compile('Previous Day')).parent
    print soup.find(text = re.compile('Next Day')).parent
    # Question 3
    print soup.find(text = re.compile('Max Temperature')).parent.parent.parent.find_all('span')[1].find('span')

# Problem 8
def prob8():
    """Loads 'Big Data dates.htm' into BeautifulSoup and uses find_all()
    and re to return a list of all tags containing links to bank data
    from September 30, 2003 to December 31, 2014.
    """
    soup = BeautifulSoup(open('federalreserve.htm'), 'html.parser')
    return soup.find_all(href = True, text = re.compile('200[3-9]|201[0-4]'))



if __name__ == '__main__':
    #prob2()
    #print  prob3()
    #print prob1('example.htm')
    #print prob4()
    #print prob6(2)
    print prob8()
    pass