# nameserver.py
"""Vol 3B: Web Tech 1 (Internet Protocols). Auxiliary file.
Drew Pearson
Math 405
1/10/17
"""

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import urlparse
import os

names = {   "Babbage": "Charles",
            "Berners-Lee": "Tim",
            "Boole": "George",
            "Cerf":"Vint",
            "Dijkstra":"Edsger",
            "Hopper":"Grace",
            "Knuth":"Donald",
            "von Neumann":"John",
            "Russel":"Betrand",
            "Shannon":"Claude",
            "Turing":"Alan"        }

class NameServerHTTPRequestHandler(BaseHTTPRequestHandler):
    """Custom HTTPRequestHandler class"""

    def do_GET(self):
        """Handle GET command"""
        self.send_response(200)

        parsed_path = urlparse.urlparse(self.path)
        try:
            params = dict([p.split('=') for p in parsed_path[4].split('&')])
        except:
            params = {}

        # Send header first
        self.send_header("Content-type","text-html")
        self.end_headers()

        # Send content to client
        try:
            if params["lastname"] == 'AllNames':
                rel_name = [(name,names[name]) for name in names.keys()]
            else:
                rel_name = [(name,names[name]) for name in names.keys() if name.startswith(params['lastname'])]
            name_formatted = str() 
            for name in rel_name:
                name_formatted += name[0] + ', ' + name[1] + '\n'
            self.wfile.write(name_formatted)

        except:
            self.wfile.write("I don\'t know that person.")
        return
    def do_PUT(self):
        self.send_response(200)
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        #print post_body
        try:
            params = dict([p.split('=') for p in post_body.split('&')])
        except:
            params = {}
        names[params['lastname']]=params['firstname']
def run():
    # Print("http server is starting...")
    server_address = ("0.0.0.0", 8000)
    httpd = HTTPServer(server_address, NameServerHTTPRequestHandler)
    print("http server is running...")
    httpd.serve_forever()

if __name__ == '__main__':
    run()