import socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


address = '0.0.0.0' # Default address that specifies the local machine and allows -connection on all interfaces
s.bind((address, 33498)) # Bind the socket to a port 33498, which was arbitrarily -chosen
s.listen(1) # Tell the socket to listen for incoming connections

size = 2048 # Block size of 20 bytes
conn, addr = s.accept() # conn is our new socket object for receiving/sending -data
print "Accepting connection from:", addr