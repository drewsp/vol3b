###Create a client socket
import socket
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip = '0.0.0.0'
port = 33498
client.connect((ip, port))

size = 2048 # Block size of 20 bytes
msg = "Trololololo, lolololololo, ahahahaha."
client.send(msg)
print "Waiting for the server to echo back the data..."
data = client.recv(size)
print "Server echoed back:", data
client.close()