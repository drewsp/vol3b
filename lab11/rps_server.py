import socket
import numpy as np
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


address = '0.0.0.0' # Default address that specifies the local machine and allows -connection on all interfaces
s.bind((address, 33487)) # Bind the socket to a port 33498, which was arbitrarily -chosen
s.listen(1) # Tell the socket to listen for incoming connections

size = 2048 # Block size of 20 bytes
conn, addr = s.accept() # conn is our new socket object for receiving/sending -data
print "Accepting connection from:", addr

while True:
    data = conn.recv(size) # Read 20 bytes from the incoming connection
    rps_list = ['rock', 'paper','scissors']
    if data not in rps_list:
    	conn.send('Invalid Move')
    	break
    server_move = np.random.randint(0,3)
    client_move = rps_list.index(data)
    #print data
    #print rps_list.index(server_move)
    matrix = np.array([[0,1,2],[2,0,1],[1,2,0]])
    outcome = ['draw','you win','you lose']
    result = matrix[server_move][client_move]
    print 'server shot: '+rps_list[server_move] +' ' +'client shot: ' + data 
    data  = str(outcome[result])
    if not data: # Terminate the connection if data stops arriving (no more -blocks to receive)
        break
    conn.send(data) # Send the data back to the client
 
conn.close()