import json
import datetime
import requests
import pandas as pd
import numpy as np
import xml.etree.ElementTree as et

from pyproj import Proj, transform
from scipy.spatial import cKDTree

from bokeh.plotting import figure, show
from bokeh.models import WMTSTileSource, ColumnDataSource
from pyproj import Proj, transform
from bokeh.io import curdoc
from bokeh.layouts import column, row, layout, widgetbox

# Problem 1
class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return {'dtype': 'datetime','year': obj.year, 'month':obj.month, 'day': obj.day, 'hour' :obj.hour, 'minute' :obj.minute, 'second':obj.second, 'microsecond':obj.microsecond}
        return json.JSONEncoder.default(self, obj)

accepted_dtypes = {'datetime': datetime.datetime}
def DateTimeDecoder(item):
    type = accepted_dtypes.get(item['dtype'], None)
    if type is not None:
        return datetime.datetime(item['year'],item['month'],item['day'],item['hour'],item['minute'],item['second'],item['microsecond'])
    return item

# Problem 2
# Provide a solution to this problem in a separate file
def prob2():
    '''
    This method should get a JSON file from
    https://data.lacity.org/resource/v87k-wgde.json
    using the requests library. It should then
    show a scatter plot of the water usage from
    2012 to 2013 organized by latitude and longitude.

    Scatter Plot:
    x-axis: longitude
    y-axis: latitude
    point size: water use (hundreds of cubic feet)
    '''
    

# Problem 3
def prob3():
    # Your code to get the answers here
    f = et.parse('books.xml')
    #print f
    root = f.getroot()
    price = 0
    for n in root.findall("book"):
        if float(n.find('price').text) > price:
            price = float(n.find('price').text)
            author = n.find('author').text
        else:
            pass

    tally = 0
    for n in root.findall("book"):
        if datetime.datetime.strptime(n.find('publish_date').text, '%Y-%m-%d') < datetime.datetime(year = 2000, month = 12, day = 1):
            tally +=1
        else:
            pass
    my_list = []
    for n in root.findall("book"):
        if 'Microsoft' in n.find('description').text:
            my_list.append(n.find('title').text)
        else:
            pass


    print "The author with the most expensive book is, " + str(author)
    print "The number of books published before Dec 1, 2000 is, " + str(tally)
    print "The books that reference Microsoft in their description are, ", my_list


# Problem 4
def convert(longitudes, latitudes):
    from_proj = Proj(init="epsg:4326")
    to_proj = Proj(init="epsg:3857")

    x_vals = []
    y_vals = []
    for lon, lat in zip(longitudes, latitudes):
        x, y = transform(from_proj, to_proj, lon, lat)
        x_vals.append(x)
        y_vals.append(y)

    return x_vals, y_vals

def prob4():
    pass

def test():
    a = datetime.datetime.now()
    print a
    s = json.dumps(a, cls = DateTimeEncoder)
    c = json.loads(s, object_hook = DateTimeDecoder)
    print c
    print type(c)

if __name__ == "__main__":
    prob3()
