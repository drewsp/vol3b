from bokeh.plotting import figure, show
from bokeh.models import WMTSTileSource, ColumnDataSource
from pyproj import Proj, transform
import requests
import numpy as np
from bokeh.io import curdoc
from bokeh.layouts import column, row, layout, widgetbox
import pandas as pd

from_proj = Proj(init="epsg:4326")
to_proj = Proj(init="epsg:3857")

data = requests.get("https://data.lacity.org/resource/v87k-wgde.json").json()
data = pd.DataFrame(data)

data = requests.get("https://data.lacity.org/resource/v87k-wgde.json").json()
for d in data:
    d['location_1'] =d['location_1'][u'coordinates']
data = pd.DataFrame(data)
data = data[['fy_12_13', 'location_1']]
data['fy_12_13'] = data['fy_12_13'].astype(int)
data['longitude'] = data['location_1'].apply(lambda x: x[0])
data['latitude'] = data['location_1'].apply(lambda x: x[1])

def convert(longitudes, latitudes):
    x_vals = []
    y_vals = []
    for lon, lat in zip(longitudes, latitudes):
        x, y = transform(from_proj, to_proj, lon, lat)
        x_vals.append(x)
        y_vals.append(y)
    return x_vals, y_vals

fig = figure(title="Los Angeles Water Usage 2012-2013", plot_width=600,
                plot_height=600, tools=["wheel_zoom", "pan"],
                x_range=(-13209490, -13155375), y_range=(3992960, 4069860),
                webgl=True, active_scroll="wheel_zoom")
fig.axis.visible = False
STAMEN_TONER_BACKGROUND = WMTSTileSource(
    url='http://tile.stamen.com/toner-background/{Z}/{X}/{Y}.png',
    attribution=(
        'Map tiles by <a href="http://stamen.com">Stamen Design</a>, '
        'under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>.'
        'Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, '
        'under <a href="http://www.openstreetmap.org/copyright">ODbL</a>'
    )
)
background = fig.add_tile(STAMEN_TONER_BACKGROUND)
data['x'],data['y'] = convert(data['longitude'],data['latitude'])
plot_data = dict(plot_x_vals = data['x'].as_matrix(), plot_y_vals = data['y'].as_matrix(),size = data['fy_12_13'].as_matrix())
cir_source_plot = ColumnDataSource(data = plot_data)
fig.circle('plot_x_vals','plot_y_vals',source = cir_source_plot, size='size', alpha =.4)
# Do whatever you need to in order to display the figure here
#print data
show(fig)

# Do whatever you need to in order to display the figure here
