# test_specs.py
"""Python Essentials: Testing.
Drew Pearson
Math 405
Feb 27
"""

import specs
from specs import *
import pytest
import numpy as np

# Problem 1: Test the addition and smallest factor functions from specs.py
def test_addition():
    assert specs.addition(1,3) == 4, "Addition failed on positive integers"
    assert specs.addition(-5,-7) == -12, "Addition failed on negative integers"
    assert specs.addition(-6,14) == 8

def test_smallest_factor():
    assert specs.smallest_factor(7) == 7
    assert specs.smallest_factor(2) == 2
    assert specs.smallest_factor(1) == 1
    assert specs.smallest_factor(4) == 2
    assert specs.smallest_factor(4) == 2
    assert specs.smallest_factor(9) == 3

# Problem 2: Test the operator function from specs.py
def test_operator():
    with pytest.raises(Exception) as excinfo:
        operator(4,0,3)
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper should be a string"

    with pytest.raises(Exception) as excinfo:
        operator(4,0,'dr')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper should be one character"

    with pytest.raises(Exception) as excinfo:
        operator(4,0,'/')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "You can't divide by zero!"

    with pytest.raises(Exception) as excinfo:
        operator(4,0,'9')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Oper can only be: '+', '/', '-', or '*'"

    assert operator(7,3,'+') == 10
    assert operator(6,3,'/') == 2
    assert operator(7,3,'*') == 21
    assert operator(7,3,'-') == 4


# Problem 3: Finish testing the complex number class
@pytest.fixture
def set_up_complex_nums():
    number_1 = specs.ComplexNumber(1, 2)
    number_2 = specs.ComplexNumber(5, 5)
    number_3 = specs.ComplexNumber(2, 9)
    return number_1, number_2, number_3

def test_complex_addition(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 + number_2 == specs.ComplexNumber(6, 7)
    assert number_1 + number_3 == specs.ComplexNumber(3, 11)
    assert number_2 + number_3 == specs.ComplexNumber(7, 14)
    assert number_3 + number_3 == specs.ComplexNumber(4, 18)

def test_complex_multiplication(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 * number_2 == specs.ComplexNumber(-5, 15)
    assert number_1 * number_3 == specs.ComplexNumber(-16, 13)
    assert number_2 * number_3 == specs.ComplexNumber(-35, 55)
    assert number_3 * number_3 == specs.ComplexNumber(-77, 36)

def test_complex_subtraction(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 - number_2 == specs.ComplexNumber(-4, -3)
    assert number_1 - number_3 == specs.ComplexNumber(-1, -7)
    assert number_2 - number_3 == specs.ComplexNumber(3, -4)
    assert number_3 - number_3 == specs.ComplexNumber(0, 0)

def test_complex_conjugate(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1.conjugate() == ComplexNumber(1,-2)
    assert number_2.conjugate() == ComplexNumber(5,-5)
    assert number_3.conjugate() == ComplexNumber(2,-9)

def test_complex_division(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1 / number_2 == specs.ComplexNumber(.3, .1)
    assert number_2 / number_3 == ComplexNumber(11./17, -7./17)

    with pytest.raises(Exception) as excinfo:
        number_2 / ComplexNumber(0, 0)
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Cannot divide by zero"



def test_complex_norm(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert number_1.norm() == np.sqrt(5)
    assert number_2.norm() == np.sqrt(50)
    assert number_3.norm() == np.sqrt(85)


def test_complex_string(set_up_complex_nums):
    number_1, number_2, number_3 = set_up_complex_nums
    assert str(number_1) == '1+2i'
    assert str(number_2) == '5+5i'
    assert str(number_3) == '2+9i'


# Problem 4: Write test cases for the Set game.

def test_set():
    with pytest.raises(Exception) as excinfo:
        play_set('hands/DNE.txt')
    assert excinfo.typename == 'IOError'

    with pytest.raises(Exception) as excinfo:
        play_set('hands/test_set1.csv')
    assert excinfo.typename == 'IOError'
    assert excinfo.value.args[0] == "File is wrong type"

    with pytest.raises(Exception) as excinfo:
        play_set('hands/13_cards.txt')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Must have 12 cards"

    with pytest.raises(Exception) as excinfo:
        play_set('hands/len5_card.txt')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Cards can only have 4 bit values"

    with pytest.raises(Exception) as excinfo:
        play_set('hands/invalid_card.txt')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "cards must be in base 3!"

    with pytest.raises(Exception) as excinfo:
        play_set('hands/duplicate_cards.txt')
    assert excinfo.typename == 'ValueError'
    assert excinfo.value.args[0] == "Can't have duplicate cards"
    assert 6 == specs.play_set("hands/lab_testset.txt")







