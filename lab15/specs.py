# specs.py
"""Python Essentials: Testing.
Drew Pearson
math 405
Feb 27, 2017
"""
import math
import itertools 

# Problem 1 Write unit tests for addition().
# Be sure to install pytest-cov in order to see your code coverage change.
def addition(a,b):
    return a + b

def smallest_factor(n):
    """Finds the smallest prime factor of a number.
    Assume n is a positive integer.
    """
    if n == 1:
        return 1
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return i
    return n


# Problem 2 Write unit tests for operator().
def operator(a, b, oper):
    if type(oper) != str:
        raise ValueError("Oper should be a string")
    if len(oper) != 1:
        raise ValueError("Oper should be one character")
    if oper == "+":
        return a+b
    if oper == "/":
        if b == 0:
            raise ValueError("You can't divide by zero!")
        return a/float(b)
    if oper == "-":
        return a-b
    if oper == "*":
        return a*b
    else:
        raise ValueError("Oper can only be: '+', '/', '-', or '*'")

# Problem 3 Write unit test for this class.
class ComplexNumber(object):
    def __init__(self, real=0, imag=0):
        self.real = real
        self.imag = imag

    def conjugate(self):
        return ComplexNumber(self.real, -self.imag)

    def norm(self):
        return math.sqrt(self.real**2 + self.imag**2)

    def __add__(self, other):
        real = self.real + other.real
        imag = self.imag + other.imag
        return ComplexNumber(real, imag)

    def __sub__(self, other):
        real = self.real - other.real
        imag = self.imag - other.imag
        return ComplexNumber(real, imag)

    def __mul__(self, other):
        real = self.real*other.real - self.imag*other.imag
        imag = self.imag*other.real + other.imag*self.real
        return ComplexNumber(real, imag)

    def __div__(self, other):
        if other.real == 0 and other.imag == 0:
            raise ValueError("Cannot divide by zero")
        bottom = (other.conjugate()*other*1.).real
        top = self*other.conjugate()
        return ComplexNumber(top.real / bottom, top.imag / bottom)

    def __eq__(self, other):
        return self.imag == other.imag and self.real == other.real

    def __str__(self):
        return "{}{}{}i".format(self.real, '+' if self.imag >= 0 else '-',
                                                                abs(self.imag))
# Problem 5: Write code for the Set game here

def play_set(file_path):
    file_name = open(file_path)
    if file_path.endswith('.txt') == False:
        raise IOError("File is wrong type")
    
    cards = file_name.read().split('\n')
    cards = cards[:-1]
    if len(cards) != 12:
        raise ValueError("Must have 12 cards")
    duplicate = [1 for i in cards if cards.count(i)>1]
    if sum(duplicate)>=1:
        raise ValueError("Can't have duplicate cards")
    for card in cards:
        if len(card) !=4:
            raise ValueError("Cards can only have 4 bit values")
    
    for card in cards:
        card = list(card)
        for value in card:
            if value != '0' and value != '1' and value != '2':
                raise ValueError('cards must be in base 3!')
    
    number_of_sets = 0

    for i in itertools.combinations(cards,3):
        check_set = list(i)
        check_set = [[int(el) for el in card] for card in check_set]
        first_col = (check_set[0][0]+check_set[1][0]+check_set[2][0])%3
        second_col = (check_set[0][1]+check_set[1][1]+check_set[2][1])%3
        third_col = (check_set[0][2]+check_set[1][2]+check_set[2][2])%3
        fourth_col = (check_set[0][3]+check_set[1][3]+check_set[2][3])%3
        if first_col+second_col+third_col+fourth_col ==0:
            number_of_sets += 1
    return number_of_sets
