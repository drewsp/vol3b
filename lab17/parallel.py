# parallel1.py

from ipyparallel import Client
client = Client()
dview = client[:]
import time
from matplotlib import pyplot as plt

def prob3(n=1000000):
    """
    Write a function that returns the mean, max, and min for n draws
    from the standard normal distribution where n is default to 1,000,000.
    Use apply_sync() to execute this function across all available engines.
    Print the results as returned by each engine.
    """
    def draw():
        import numpy as np
        a = np.random.randn(n)
        mean = np.mean(a)
        my_max = np.max(a)
        my_min = np.min(a)
        return mean, my_max, my_min
    results = dview.apply_sync(draw)
    #print results
    mean = []
    my_max = []
    my_min = []
    for i in xrange(len(results)):
        mean.append(str(results[i][0]))
        my_max.append(str(results[i][1]))
        my_min.append(str(results[i][2]))
    #my_max = results[:,1]
    #my_min = results[:,2]
    print mean, my_max, my_min

def prob4():
    """
    Time the function from the previous problem in parallel and serially. Run
    apply_sync() on the function to time in parallel. To time the function
    serially, run the function in a for loop n times, where n is the number
    of engines on your machine. Print the results.
    """
    n_list = [1000000, 5000000, 10000000, 15000000]
    for n in n_list:
        def draw():
            import numpy as np
            a = np.random.randn(n)
            mean = np.mean(a)
            my_max = np.max(a)
            my_min = np.min(a)
            return mean, my_max, my_min
        start_time_serial = time.time()
        for i in xrange(0,4):
            draw()
        end_time_serial = time.time()
        print "for n=" +str(n) + " serially took " + str(end_time_serial-start_time_serial)
        start_time_parallel = time.time()
        results = dview.apply_sync(draw)
        end_time_parallel = time.time()
        print "for n=" +str(n) + " parallel took " + str(end_time_parallel-start_time_parallel) +"\n"


def prob5(N):
    """
    Accept an integer N that represents the number of draws to take from the
    normal distribution for the distribution X. Take 500,000 draws from this
    distribution and plot a histogram of the results. Split the load evenly
    among all available engines and make the function flexible to the number
    of engines running.
    """
    def draw(_):
        import numpy as np
        a = np.random.randn(N)
        return np.max(a)
    PDF = dview.map_sync(draw, range(500000))
    plt.hist(PDF, 50, normed=True)
    plt.show()


def parallel_trapezoidal_rule(f, a, b, n=200):
    """
    Write a function that accepts a function handle, f, bounds of integration,
    a and b, and a number of points to use, n. Split the interval of
    integration evenly among all available processors and use the trapezoidal
    rule to numerically evaluate the integral over the interval [a,b].

    Parameters:
        f (function handle): the function to evaluate
        a (float): the lower bound of integration
        b (float): the upper bound of integration
        n (int): the number of points to use, defaults to 200
    Returns:
        value (float): the approximate integral calculated by the
            trapezoidal rule
    """
    h = (a+b)/float(n)
    def area(i):
        x_i = a+h*i
        x_i_1 = a+h*(i+1)
        return h/2*(f(x_i_1)+f(x_i))
    return sum(dview.map_sync(area, range(n)))

#print client.ids

if __name__ == '__main__':
    #print prob4()
    #clients.ids
    #prob5(10000)
    def f(x):
        return x**3+4*x-3
    print parallel_trapezoidal_rule(f,0,3)