01/20/17 18:40

Basic Logistic Regression (6 points):
Score += 6

Regularized Logistic Regression (6 points):
Score += 6

Compare Basic to Regularized (3 points):
Score += 3

Multinomial Logistic Regression (6 points):
Score += 6

Compare to Sklearn (3 points):
Score += 3

Code Quality (6 points):
Score += 6

Total score: 30/30 = 100.0%

Excellent!


Comments:
	Good work including comments. You don't need to have a comment for every line, especially  if the line is self-explanatory. It will also look a little cleaner if you have a space after the hash symbol.

--------------------------------------------------------------------------------

